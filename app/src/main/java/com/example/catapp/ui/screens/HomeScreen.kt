package com.example.catapp.ui.screens

import android.content.Intent
import android.widget.Toast
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ExpandLess
import androidx.compose.material.icons.filled.ExpandMore
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import com.example.catapp.CatDetail
import com.example.catapp.R
import com.example.catapp.model.CatDto
import com.skydoves.landscapist.ImageOptions
import com.skydoves.landscapist.glide.GlideImage
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json

@Composable
fun HomeScreen(
    catsuistate: CatsUiState,
    modifier: Modifier = Modifier,
    setSelectCountry: (String) -> Unit
) {
    when (catsuistate) {
        is CatsUiState.Loading -> LoadingScreen(modifier)
        is CatsUiState.Success -> ResultScreen(
            catsuistate.cats,
            catsuistate.SelectCountry,
            setSelectCountry,
            modifier)
        is CatsUiState.Error -> ErrorScreen(modifier)
    }
}

@Composable
fun LoadingScreen(modifier: Modifier = Modifier) {
    Box(
        contentAlignment = Alignment.Center,
        modifier = modifier
            .fillMaxSize()
            .background(color = MaterialTheme.colors.background)
    ) {

        GlideImage(
            imageModel = { (R.drawable.loading) }, modifier = Modifier
                .height(375.dp)
                .fillMaxWidth(),
            imageOptions = ImageOptions(contentScale = ContentScale.Crop)
        )
    }
}

@Composable
fun ErrorScreen(modifier: Modifier = Modifier) {
    Box(
        contentAlignment = Alignment.Center,
        modifier = modifier
            .fillMaxSize()
            .background(color = MaterialTheme.colors.background)
    ) {
        Column(Modifier.padding(8.dp)) {
            GlideImage(
                imageModel = { (R.drawable.error) }, modifier = Modifier
                    .height(400.dp)
                    .fillMaxWidth(),
                imageOptions = ImageOptions(contentScale = ContentScale.Crop)
            )
            Spacer(modifier = Modifier.height(10.dp))
            Text(stringResource(id = R.string.loading_failed), color = MaterialTheme.colors.secondary, modifier=Modifier.padding(start = 35.dp), style = MaterialTheme.typography.body2)
        }

    }
}

@Composable
fun ResultScreen(gatitosInfo: List<CatDto>, selectCountry: String, setSelectCountry: (String) -> Unit, modifier: Modifier = Modifier) {
    Column(modifier) {
        var gatitosInfoListaFiltrada= gatitosInfo
        Country(selectCountry, setSelectCountry)
        if(selectCountry!="SelectCountry"){
            gatitosInfoListaFiltrada=gatitosInfo.filter { it.countryCode==selectCountry }

        }
        Spacer(modifier=Modifier.height(2.dp))
        CatList(gatitosInfoListaFiltrada, Modifier.background(color = MaterialTheme.colors.onBackground))

    }
}

@Composable
fun Country(selectCountry: String, setSelectCountry: (String) -> Unit) {
    val listItems =
        arrayOf(
            "SelectCountry",
            "EG",
            "US",
            "CA",
            "ES",
            "BR",
            "GR",
            "AE",
            "AU",
            "FR",
            "GB",
            "MM",
            "CY",
            "RU",
            "CN",
            "JP",
            "TH",
            "IM",
            "NO",
            "IR",
            "SP",
            "SO",
            "TR"
        )
    val contextForToast = LocalContext.current.applicationContext

    // state of the menu
    var expanded by remember {
        mutableStateOf(false)
    }

    Row(
        modifier = Modifier
            .fillMaxWidth()
            .background(color = MaterialTheme.colors.background)
            .padding(5.dp),
        verticalAlignment = Alignment.Top,
        horizontalArrangement = Arrangement.Start,

        ) {

        ButtonOne(
            expanded = expanded,
            onClick = { expanded = !expanded },
            selectCountry=selectCountry,
        )

        // drop down menu
        DropdownMenu(
            expanded = expanded,
            onDismissRequest = {
                expanded = false
            },
            modifier = Modifier
                .height(250.dp)
                .background(color = MaterialTheme.colors.primaryVariant)
        ) {
            // adding items
            listItems.forEach { itemValue ->
                DropdownMenuItem(
                    onClick = {
                        setSelectCountry(itemValue)
                        Toast.makeText(contextForToast, "Country: $itemValue", Toast.LENGTH_SHORT)
                            .show()
                        expanded = false
                    },
                    enabled = (itemValue != selectCountry)
                ) {
                    Text(text = itemValue,
                        color = MaterialTheme.colors.secondary)
                }
            }
        }

    }
}

@Composable
fun ButtonOne(
    expanded: Boolean,
    onClick: () -> Unit,
    selectCountry: String,
    modifier: Modifier = Modifier

) {

    Button(onClick = onClick,colors = ButtonDefaults.buttonColors(backgroundColor = MaterialTheme.colors.primaryVariant)) {

        Text(text = selectCountry,
            color = MaterialTheme.colors.secondary,
            textAlign = TextAlign.Center)

        Icon(
            imageVector = if (expanded) Icons.Filled.ExpandLess else Icons.Filled.ExpandMore,
            tint = MaterialTheme.colors.secondary,
            contentDescription = "IconButton"
        )

    }




}


@Composable
fun CatList(catiList: List<CatDto>, modifier: Modifier = Modifier) {
    val mContext= LocalContext.current
    LazyColumn(modifier = modifier) {
        items(items = catiList) { catDto ->
            Button(onClick = { val intent=Intent(mContext, CatDetail::class.java)
                val cattext= Json.encodeToString(catDto)
                intent.putExtra("catInfo", cattext)
                mContext.startActivity(intent) },colors = ButtonDefaults.buttonColors(backgroundColor = MaterialTheme.colors.background)) {

                CatCard(
                    gatitoPhoto = catDto,
                    modifier = Modifier.background(color = MaterialTheme.colors.background)
                )
            }
        }
    }
}


@Composable
fun CatCard(gatitoPhoto: CatDto, modifier: Modifier = Modifier) {

    Card(
        modifier = modifier,
        border = BorderStroke(
            4.dp,
            color = MaterialTheme.colors.primary
        ),
        elevation = 5.dp,

    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(5.dp)
                .background(color = MaterialTheme.colors.background),
            horizontalArrangement = Arrangement.Start,
            verticalAlignment = Alignment.CenterVertically,

            ) {
            GlideImage(
                imageModel = { gatitoPhoto.image!!.url }, modifier = Modifier
                    .size(200.dp),
                imageOptions = ImageOptions(contentScale = ContentScale.Crop)
            )
            Spacer(modifier=Modifier.width(8.dp))
            Column( modifier = Modifier
                .background(color=MaterialTheme.colors.background),) {
                gatitoPhoto.name?.let{
                    Text(
                        text = it,
                        color = MaterialTheme.colors.secondary
                    )
                }

                gatitoPhoto.description?.let {
                    Text(
                        text = it,
                        color = MaterialTheme.colors.primary,
                        overflow = TextOverflow.Ellipsis,
                        maxLines = 2
                    )
                }

            }

        }
    }
}







/*@Preview(showBackground = true)
@Composable
fun LoadingScreenPreview() {
    CatappTheme {
        LoadingScreen()
    }
}

@Preview(showBackground = true)
@Composable
fun ErrorScreenPreview() {
    CatappTheme {
        ErrorScreen()
    }
}

@Preview(showBackground = true)
@Composable
fun ResultScreenPreview() {
    CatappTheme {
        //   ResultScreen(stringResource(R.string.result))
    }
}

@Preview
@Composable
private fun GatitosCardPreview() {


}*/
