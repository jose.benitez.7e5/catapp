package com.example.catapp.network
import com.example.catapp.model.CatDto
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import kotlinx.serialization.json.Json
import okhttp3.MediaType.Companion.toMediaType
import retrofit2.Retrofit
import retrofit2.http.GET
import retrofit2.http.Headers


private const val BASE_URL =
    "https://api.thecatapi.com/v1/"

private const val token = "live_yYhvAEI0RxTLDLKR3YmGIxk5jXGKMFPBRJ8bAJnCkzO07rtIRumXJvIWT9rH4ls3"


private val jsonIgnored = Json { ignoreUnknownKeys = true }


private val retrofit = Retrofit.Builder()
    .addConverterFactory(jsonIgnored.asConverterFactory("application/json".toMediaType()))
    .baseUrl(BASE_URL)
    .build()

interface CatsApiService {
    @Headers("x-api-key: $token")
    @GET("breeds")
    suspend fun getCatsInfo(): List<CatDto>

}

object CatsApi {
    val retrofitService: CatsApiService by lazy {
        retrofit.create(CatsApiService::class.java)
    }
}
