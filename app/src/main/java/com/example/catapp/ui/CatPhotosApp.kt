package com.example.catapp.ui

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.catapp.R
import com.example.catapp.ui.screens.CatsViewModel
import com.example.catapp.ui.screens.HomeScreen
import com.example.catapp.ui.theme.CatappTheme

@Composable
fun CatPhotosApp(modifier: Modifier = Modifier) {
    Scaffold(
        modifier = modifier
            .fillMaxSize()
            .background(color = MaterialTheme.colors.onBackground),
        topBar = { TopAppBar(title= {Text(stringResource(R.string.app_name), color = MaterialTheme.colors.background) })}
    ) {
        CatappTheme() {
            Surface(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(it),
                color = MaterialTheme.colors.onBackground
            ) {
                val catsViewModel: CatsViewModel = viewModel()
                HomeScreen(catsuistate = catsViewModel.catsuistate,
                    setSelectCountry = { catsViewModel.SetSelectCountry(it) })
            }
        }
    }
}
