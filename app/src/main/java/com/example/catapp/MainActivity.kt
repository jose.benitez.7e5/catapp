package com.example.catapp

import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import com.example.catapp.ui.theme.CatappTheme
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.text.input.*
import androidx.compose.ui.unit.dp
import com.skydoves.landscapist.ImageOptions
import com.skydoves.landscapist.glide.GlideImage

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            CatappTheme {

                LogIn(modifier = Modifier
                    .fillMaxSize()
                    .background(color = MaterialTheme.colors.background))
//                }
            }
        }
    }
}



@Composable
fun LogIn(modifier: Modifier) {

    Column(modifier, verticalArrangement = Arrangement.Center, horizontalAlignment = Alignment.CenterHorizontally){

        GlideImage(
            imageModel = { (R.drawable.cutecat) }, modifier = Modifier
                .height(300.dp),
            imageOptions = ImageOptions(contentScale = ContentScale.Crop)
        )
        Spacer(modifier = Modifier.height(30.dp))
        Email()
        Spacer(modifier = Modifier.height(30.dp))
        Password()
        Spacer(modifier = Modifier.height(35.dp))
        LoginButton()

    }
}

@Composable
fun Email() {
    var textMail by remember { mutableStateOf(TextFieldValue("")) }
    OutlinedTextField(
        value = textMail,
        leadingIcon = { Icon(imageVector = Icons.Default.Email, contentDescription = "emailIcon", tint = MaterialTheme.colors.secondary,) },
        keyboardOptions = KeyboardOptions.Default.copy( imeAction = ImeAction.Next, keyboardType = KeyboardType.Email),
        onValueChange = {
            textMail = it
        },
        label = { Text(text = "Email address",color = MaterialTheme.colors.secondary) },
        placeholder = { Text(text = "Enter your e-mail",color = MaterialTheme.colors.primary) },
        colors = TextFieldDefaults.outlinedTextFieldColors(
            focusedBorderColor = MaterialTheme.colors.secondary,
            unfocusedBorderColor = MaterialTheme.colors.primary, textColor = MaterialTheme.colors.onSecondary)

    )
}

@Composable
fun Password() {
    var password by remember { mutableStateOf(TextFieldValue("")) }
    var passwordVisible by remember { mutableStateOf(false) }
    val focusManager = LocalFocusManager.current

    return OutlinedTextField(
        value = password,
        onValueChange = { password = it },
        label = { Text("Password",color = MaterialTheme.colors.secondary) },
        placeholder = { Text("Enter password",color = MaterialTheme.colors.primary) },
        visualTransformation = if (passwordVisible) VisualTransformation.None else PasswordVisualTransformation(),
        keyboardOptions = KeyboardOptions.Default.copy( imeAction = ImeAction.Done,keyboardType = KeyboardType.Password),
        keyboardActions = KeyboardActions(
            onDone = {focusManager.clearFocus()}
        ),
        colors = TextFieldDefaults.outlinedTextFieldColors(
            focusedBorderColor = MaterialTheme.colors.secondary,
            unfocusedBorderColor = MaterialTheme.colors.primary, textColor = MaterialTheme.colors.onSecondary),
        trailingIcon = {
            val image = if (passwordVisible)
                Icons.Filled.Visibility
            else Icons.Filled.VisibilityOff

            val description = if (passwordVisible) "Hide password" else "Show password"

            IconButton(onClick = {passwordVisible = !passwordVisible}){
                Icon(imageVector  = image, description, tint = MaterialTheme.colors.secondary,)
            }
        }
    )
}

@Composable
fun LoginButton(
) {
    val mContext= LocalContext.current
    Button(colors = ButtonDefaults.buttonColors(backgroundColor = MaterialTheme.colors.primaryVariant),onClick = {
        val intent= Intent(mContext, CatActivityStart::class.java)
        mContext.startActivity(intent)
    }) {
        Text(
            text = ("LOG IN"),
            color = MaterialTheme.colors.secondary,
            modifier = Modifier
                .padding(8.dp)
        )
    }

}
