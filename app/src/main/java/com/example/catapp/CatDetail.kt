package com.example.catapp
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.example.catapp.model.CatDto
import com.example.catapp.ui.theme.CatappTheme
import com.skydoves.landscapist.ImageOptions
import com.skydoves.landscapist.glide.GlideImage
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json

class CatDetail : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val intent = intent
        val cattext = intent.getStringExtra("catInfo")
        val catDto = cattext?.let { Json.decodeFromString<CatDto>(it) }
        setContent {
            CatappTheme() {

                Surface(
                    modifier = Modifier.fillMaxSize(),

                ) {
                    Column(
                        modifier = Modifier
                            .fillMaxSize()
                            .background(color = MaterialTheme.colors.background)
                            .padding(5.dp)
                            .verticalScroll(rememberScrollState()),
                        verticalArrangement = Arrangement.Center,
                        horizontalAlignment = Alignment.CenterHorizontally,

                        ) {
                        if (catDto != null) {
                            FullCatCard(catDto)
                        }
                    }
                }

            }
        }
    }
}


@Composable
fun FullCatCard(catInfo: CatDto) {
    Card(
        border = BorderStroke(
            4.dp,
            color = MaterialTheme.colors.primary
        ),
        elevation = 5.dp,
        backgroundColor = MaterialTheme.colors.background
    ) {
        Column() {
            GlideImage(
                imageModel = { catInfo.image!!.url }, modifier = Modifier
                    .height(400.dp)
                    .fillMaxWidth(),
                imageOptions = ImageOptions(contentScale = ContentScale.Crop)
            )
            Description(catInfo)
        }

    }


}

@Composable
fun Description(catInfo: CatDto) {
    Column(modifier = Modifier.padding(4.dp)) {
        Spacer(modifier = Modifier.height(10.dp))
        Text(
            text = "Description: ",
            color = MaterialTheme.colors.primary,
            modifier = Modifier
                .padding(7.dp)
        )
        Divider(
            color = MaterialTheme.colors.primary,
            thickness = 2.dp,
            modifier = Modifier.padding(start = 8.dp, end = 20.dp)
        )
        Spacer(modifier = Modifier.height(10.dp))
        catInfo.description?.let {
            Text(
                text = it,
                color = MaterialTheme.colors.secondary,
                modifier = Modifier
                    .padding(start = 16.dp, end = 16.dp),
                textAlign = TextAlign.Justify
            )
        }
        Spacer(modifier = Modifier.height(10.dp))

        Text(
            text = "Breeds: ",
            color = MaterialTheme.colors.primary,
            modifier = Modifier
                .padding(8.dp)
        )
        Divider(
            color = MaterialTheme.colors.primary,
            thickness = 2.dp,
            modifier = Modifier.padding(start = 5.dp, end = 15.dp)
        )

        Spacer(modifier = Modifier.height(10.dp))
        catInfo.name?.let {
            Text(
                text = it,
                color = MaterialTheme.colors.secondary,
                modifier = Modifier
                    .padding(start = 16.dp, end = 16.dp),
                textAlign = TextAlign.Justify
            )
        }
        Spacer(modifier = Modifier.height(10.dp))
        Text(
            text = "Temperament: ",
            color = MaterialTheme.colors.primary,
            modifier = Modifier
                .padding(8.dp)
        )
        Divider(
            color = MaterialTheme.colors.primary,
            thickness = 2.dp,
            modifier = Modifier.padding(start = 5.dp, end = 15.dp)
        )

        Spacer(modifier = Modifier.height(10.dp))
        catInfo.temperament?.let {
            Text(
                text = it,
                color = MaterialTheme.colors.secondary,
                modifier = Modifier
                    .padding(start = 12.dp, end = 12.dp),
                textAlign = TextAlign.Justify
            )
        }

        Spacer(modifier = Modifier.height(10.dp))
        Text(
            text = "Wiki: ",
            color = MaterialTheme.colors.primary,
            modifier = Modifier
                .padding(7.dp)
        )
        Divider(
            color = MaterialTheme.colors.primary,
            thickness = 2.dp,
            modifier = Modifier.padding(start = 5.dp, end = 15.dp)
        )

        Spacer(modifier = Modifier.height(10.dp))
        catInfo.wikipediaUrl?.let {
            Wikipediabutton(Url = catInfo.wikipediaUrl!!)
        }
        Spacer(modifier = Modifier.height(10.dp))
    }

}

@Composable
fun Wikipediabutton(Url: String) {
    val context = LocalContext.current
    val intent = remember {
        Intent(Intent.ACTION_VIEW, Uri.parse(Url))
    }
    Box(modifier = Modifier.clickable { context.startActivity(intent) }) {
        Text(
            text = Url,
            color = MaterialTheme.colors.secondary,
            modifier = Modifier
                .padding(start = 15.dp, end = 15.dp),
            textAlign = TextAlign.Justify
        )
    }
}
