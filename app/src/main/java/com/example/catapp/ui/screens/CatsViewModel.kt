package com.example.catapp.ui.screens

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.catapp.model.CatDto
import com.example.catapp.network.CatsApi
import kotlinx.coroutines.launch
import retrofit2.HttpException
import java.io.IOException

sealed interface CatsUiState {
    data class Success(val cats: List<CatDto>, val SelectCountry: String) : CatsUiState
    object Error : CatsUiState
    object Loading : CatsUiState
}

class CatsViewModel : ViewModel() {
    var catsuistate: CatsUiState by mutableStateOf(CatsUiState.Loading)
        private set

    init {
        getCatDto()
    }

    fun getCatDto() {
        viewModelScope.launch {
            catsuistate = try {
                val listResult = CatsApi.retrofitService.getCatsInfo()
                CatsUiState.Success(listResult, "Select Country")
            } catch (e: IOException) {
                CatsUiState.Error
            } catch (e: HttpException) {
                CatsUiState.Error
            }
        }
    }

    fun SetSelectCountry(otherCountry: String){
        if(catsuistate is CatsUiState.Success){
            catsuistate=(catsuistate as CatsUiState.Success).copy(SelectCountry = otherCountry)
        }
    }
}