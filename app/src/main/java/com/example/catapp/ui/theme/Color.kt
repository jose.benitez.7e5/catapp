package com.example.catapp.ui.theme

import androidx.compose.ui.graphics.Color

val lavander = Color(0xFFAD82E2)
val pinky = Color(0xFFCF56BB)
val Orange = Color(0xFFF78558)
val bluesoft = Color(0xFF75A3F3)
val Black = Color(0xFF000000)