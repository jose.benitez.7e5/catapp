package com.example.catapp
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.Surface
import androidx.compose.ui.Modifier
import com.example.catapp.ui.CatPhotosApp
import com.example.catapp.ui.theme.CatappTheme

class CatActivityStart : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            CatappTheme {

                Surface(
                    modifier = Modifier.fillMaxSize(),

                ) {
                    CatPhotosApp()

                }
            }
        }
    }
}